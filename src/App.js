import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './containers/home';
import Tasks from './containers/tasks';

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path='/' component={Home}/>
                    <Route path='/tasks' component={Tasks}/>
                </div>
            </Router>
        )
    }
}

export default App;
