import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './home.css';

export default class Home extends Component {
    render() {
        return (
            <div className="home">
                <h1 className="home-title">WASTE MANAGEMENT</h1>
                <p className="home-description">Project management as a service</p>
                <Link to='/tasks'>
                    <button type="button" className="btn btn-light home-button">Get Started</button>
                </Link>
            </div>
        )
    }
}